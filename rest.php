<?php
/*
   Blake Wasung
   03/24/2017
 */

//sends back json data
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");

include ('dbconnection.php');

function internal_error($msg){
    header("HTTP/1.1 500 Internal Error");
    $response['msg'] = "unable to connect to data base";
    print json_encode($response);
    odbc_close($GLOBALS['conn']);
    exit();
}

if(!isset($conn)){
    internal_error('unable to connect to data base');
}

function getVal($val){
    if(isset($val)){
        return htmlspecialchars($val);
    }
    return;
}

function bad_request($msg){
    header("HTTP/1.1 400 Bad Request");
    $response['msg'] = $msg;
    print json_encode($response);
    odbc_close($GLOBALS['conn']);
    exit();
}

function not_found($msg){
    header("HTTP/1.1 404 Not Found");
    $response['msg'] = $msg;
    print json_encode($response);
    odbc_close($GLOBALS['conn']);
    exit();
}

function success($msg){
    header("HTTP/1.1 200 OK");
    $response['msg'] = $msg;
    print json_encode($response);
    odbc_close($GLOBALS['conn']);
    exit();
}

function successData($msg, $info){
    header("HTTP/1.1 200 OK");
    $response['msg'] = $msg;
    $response['info'] = $info;
    print json_encode($response);
    odbc_close($GLOBALS['conn']);
    exit();
}

function user(){
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            $user = getVal($_GET['rfid']);

            if(isset($user)){

				$info = array();
				$stmt = odbc_prepare($GLOBALS['conn'],"SELECT * FROM USERS WHERE UserRFID = ?");
				if(isset($stmt)){
					$info['stmt'] = 'working';
				}
				$result = odbc_execute($stmt, array($user));
				if(isset($result)){
					$info['res']= 'has results';
				}

                $info['Unique_ID']="";
                if($row = odbc_fetch_array($stmt)){//was $result
                    $info = $row;
                }else{
					success("user $user not found");
				}
				
                successData("user found", $info);
            }else{
                bad_request('RFID value not included');
            }
            break;
        case 'POST':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            $uid = getVal($data['unique_id']);
            if(!isset($uid)){
                success("unique_id must be filled in");
            }
            success('user added');
            //TODO: add user
            break;
        case 'PUT':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            success('user updated');
            //TODO: update user
            break;
        case 'DELETE':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            $rfid = getVar($data.RFID);
            success('user deleted');
            //TODO: delete user
            //$stmt = $GLOBALS['conn']->prepare
            $stmt = ("DELETE FROM USERS WHERE ?=?");
            //$stmt->bind_param('ss', 'UserRFID', $data.rfid);
            //$stmt->execute();
            //$stmt->close();
            break;
        default:
            bad_request($_SERVER['REQUEST_METHOD'].' is not supported');
    }
}
function computer(){
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(isset($_GET['rfid'])){
                $computer = getVal($_GET['rfid']);
                //TODO:make call to DB
				$info = array();
				$stmt = odbc_prepare($GLOBALS['conn'],"SELECT * FROM TECHNOLOGY WHERE RFID = ?");
				if(isset($stmt)){
					$info['stmt'] = 'working';
				}
				$result = odbc_execute($stmt, array($computer));
				if(isset($result)){
					$info['res']= 'has results';
				}

                if($row = odbc_fetch_array($stmt)){//was $result
                    $info = $row;
                }else{
					success("computer $computer not found");
				}
				
                successData("computer found", $info);
            }else{
                //bad_request('RFID value must be included');
				$info = array();
				$i = 0;
				$stmt = odbc_prepare($GLOBALS['conn'], "SELECT * FROM TECHNOLOGY");
				$result = odbc_execute($stmt);
				while($row = odbc_fetch_array($stmt)){
					$info[$i++] = $row;
				}
				successData("list of computers", $info);
            }
            break;
        case 'POST':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            $rfid = getVal($data['rfid']);
            if(!isset($rfid)){
                bad_request("the field 'rfid' is required");
            }
			$tag = getVal($data.serviceTag);
			$type = getVal($data.type);
			$model = getVal($data.model);
			$location = getVal($data.location);
			$ghz = getVal($data.ghz);
			$ram = getVal($data.ram);
			$monitor = getVal($data.monitor);
			$prodNum = getVal($data.productNumber);
			$warr = getVal($data.warranty);
			$purch = getVal($data.purchaseDate);
			$cost = getVal($data.cost);
			$description = getVal($data.description);
			$status = getVal($data.status);
			
			$stmt = odbc_prepare($GLOBALS['conn'], 'INSERT INTO TECHNOLOGY VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
			$result = odbc_execute($stmt, array($rfid,$tag, $type, $model, $location, $ghz, $ram, $monitor, $prodNum, $warr, $purch, $cost, $description, $status));
            if(odbc_fetch_array($stmt)){
			    success('computer updated');
			}else{
				success('something went wrong. please try again');
			}
	        
			success('something went wrong, please try again');
            break;
        case 'DELETE':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            $rfid = getVal($data.rfid);
            if(!isset($rfid)){
                bad_request("the field 'rfid' is required");
            }
            success('computer deleted');
            //TODO: delete computer
            break;
        case 'PUT':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            $rfid = getVal($data.rfid);
            if(!isset($rfid)){
                bad_request("the field 'rfid' is required");
            }
            success('computer added');
            //TODO: add new computer
            //$addComp = conn->prepare(string);
            $addComp = "INSERT INTO TECHNOLOGY VALUES (RFID, SerialServiceTag, DeviceType, DeviceModel, DeviceLocation, GHZ, RAM_GB, Monitor, Product_Number, Warranty_Expiration, Purhase_Date, Cost, Description, Status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            //$addComp->bind_param('sssssssssssssss', $rfid, $serial, etc...);
            break;
        default:
            bad_request($_SERVER['REQUEST_METHOD'].' is not supported');
    }
}


function logSystem(){
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
		    $computer = getVal($_GET['computer']);
			$user = getVal($_GET['user']);
			$info = array();
			$i = 0;
			if(!empty($computer)){
				$stmt = odbc_prepare($GLOBALS['conn'], 'SELECT * FROM LOG WHERE RFID = ?');
				$result = odbc_execute($stmt, array($computer));
			}else if(!empty($user)){
				$stmt = odbc_prepare($GLOBALS['conn'], 'SELECT * FROM LOG WHERE UNIQUE_ID = ?');
				$result = odbc_execute($stmt, array($user));
			}
			
			if(!empty($computer)&&!empty($user)){
				while($row = odbc_fetch_array($stmt)){
					$info[$i++] = $row;
				}
			}else{
				//get all
				$stmt = odbc_prepare($GLOBALS['conn'], "SELECT * FROM LOG");
				$result = odbc_execute($stmt);
				while($row = odbc_fetch_array($stmt)){
					$info[$i++] = $row;
				}
			}
			successData('List of logs', $info);
            break;
        case 'POST':
            $content = file_get_contents('php://input');
            $data = json_decode($content, true);
            if(empty($data['user'])||empty($data['computer'])){
                bad_request("User and Computer fields must be filled in.");
            }
            try{
                $user = getVal($data['user']);
                $computer = getVal($data['computer']);
				$stmt = odbc_prepare($GLOBALS['conn'], "SELECT * FROM LOG WHERE UNIQUE_ID = ? AND RFID = ? AND CHECKINTIMESTAMP IS NULL");
				$result = odbc_execute($stmt, array($user, $computer));
				
				if(odbc_fetch_array($stmt)){
					//add check in time
					$stmt2 = odbc_prepare($GLOBALS['conn'], 'UPDATE LOG SET CHECKINTIMESTAMP = CURRENT_TIMESTAMP WHERE RFID = ? AND UNIQUE_ID = ? AND CHECKINTIMESTAMP IS NULL');
					$result2 = odbc_execute($stmt2, array($computer, $user));
					$stmt3 = odbc_prepare($GLOBALS['conn'], 'UPDATE TECHNOLOGY SET DEVICELOCATION = ? WHERE RFID = ?');
					$result3 = odbc_execute($stmt3, array('Inventory', $computer));
					if($result2&&$result3){
						success("$user has checked in computer $computer");
					}else{
						success("Something went wrong, please try again case 1");
					}
				}else{
					//insert
					$stmt2 = odbc_prepare($GLOBALS['conn'], 'INSERT INTO LOG (RFID, UNIQUE_ID, CHECKOUTTIMESTAMP) VALUES (?,?, CURRENT_TIMESTAMP)');
					$result2 = odbc_execute($stmt2, array($computer, $user));
					$stmt3 = odbc_prepare($GLOBALS['conn'], "UPDATE TECHNOLOGY SET DEVICELOCATION = ? WHERE RFID = ?");
					$result3 = odbc_execute($stmt3, array($user, $computer));
					if($result2&&$result3){
						success("$user has checked out computer $computer");
					}else{
						success("Something went wrong, please try again case 2");
					}
				}

                success("post was made");
            }catch(Exception $err){
                header("HTTP/1.1 500 Internal Error");
                $response['msg'] = "Something went wrong. Please try again";
                print json_encode($response);
                exit();
            }
            break;
        default:
            bad_request($_SERVER['REQUEST_METHOD'].' is not supported');
    }
}

$object = explode("/",$_SERVER['REQUEST_URI']);
$last_item=NULL;
foreach($object as $val){
    if($last_item == 'rest.php'){
        $switch = explode('?', $val);
    }
    $last_item = $val;
}
switch($switch[0]){
    case 'computer':
        computer();
        break;
    case 'user':
        user();
        break;
    case 'log':
        logSystem();
        break;
    default:
        not_found($switch);
}
?>
