<?php
/*
Written by Blake Wasung
ISA 406
03/14/2017
*/

// Load the settings from the central config file
require_once 'config.php';
// Load the CAS lib
require_once 'CAS.php';

// Enable debugging
phpCAS::setDebug();

// Initialize phpCAS
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);

// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();

// force CAS authentication
phpCAS::forceAuthentication();
// at this step, the user has been authenticated by the CAS server
// and the user's login name can be read with phpCAS::getUser().

// logout if desired
if (isset($_REQUEST['logout'])) {
        phpCAS::logout();
}

?>

<html>
<head>
<TITLE>FSB Asset Check Out</TITLE>
<title>RFID System</title>
<link href="favicon.ico" rel="Shortcut Icon"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="rfid.js"></script>
<link rel="stylesheet" type="text/css" href="asset_checkout.css">
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr align="left" valign="top"> 
    <td width="145" bgcolor="#666666" background="i_internal_leftnav_bg.gif"> 
      <table width="145" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="i_subpage_logo1.jpg" width="145" height="81"></td>
        </tr>
        <tr> 
          <td><img src="i_internal_leftnav_top.gif" width="145" height="21"></td>
        </tr>
        <tr> 
          <td align="left" valign="top"> 
            <table border="0" cellspacing="0" cellpadding="0">
              <tr align="left" valign="middle"> 
                <td width="10"><img src="spacer_trans.gif" width="10" height="35"></td>
                <td class="title" width="129">FSB</td>
              </tr>
              <tr align="left" valign="middle"> 
                <td colspan="2" bgcolor="#FFFFFF"><img src="spacer_white.gif" width="139" height="1"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td align="left" valign="top" class="navLeft"> 
            <table border="0" cellspacing="4" cellpadding="0" width="140">
              <tr> 
                <td width="25">&nbsp;</td>
                <td><a class="navLeft" href="FSB_checkout_log.php">Check-Out Log</a></td>  
              </tr>
              <tr> 
                <td width="25"><img src="spacer_trans.gif" width="25" height="5"></td>
                <td><a class="navLeft"  href="FSB_currently_checked_out.php">Currently Checked-Out</a></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td align="left" valign="top">&nbsp; </td>
        </tr>
<tr><td colspan="2">&nbsp;</td></tr>
              <tr align="left" valign="middle"> 
                <td colspan="2" bgcolor="#FFFFFF"><img src="spacer_white.gif" width="139" height="1"></td>
              </tr>
<tr><td colspan="2" height="100">&nbsp;</td></tr>

      </table>
    </td>
    <td width="100%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="left" valign="top"> 
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
              <tr> 
                <td align="left" valign="top" width="107"><img src="i_subpage_logo2.jpg" width="106" height="81"></td>
                <td align="left" valign="top" width="100%"><img src="spacer_white.gif" width="200" height="5"></td>
                <td align="right" valign="top" width="100%"> 

                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="left" valign="top"> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr align="left" valign="top"> 
                      <td width="17"><img src="spacer_trans.gif" width="17" height="35"></td>
                      <td align="left" valign="middle" class="header">FSB Asset Checkout System</td>
                    </tr>
                  </table>
                  </td>
              </tr>
              <tr>
                <td align="left" valign="top" bgcolor="#666666"><img src="spacer_white.gif" width="17" height="1"></td>
              </tr>
              <tr> 
                <td> 
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr align="left" valign="top"> 
                      <td colspan="3"><img src="spacer_trans.gif" width="17" height="8"> 
                      </td>
                    </tr>
                    <tr align="left" valign="top"> 
                      <td width="17" rowspan="2"><img src="spacer_trans.gif" width="17" height="5"></td>
                      
                      <td width="100%">
                      
                          <div id="loginbox">
                            <form name="login" id="login" class="login" method="post" action="">
                            <div>
                                <p>
                               <label class="login" style = "font-size:20px">Unique ID<br/>
                               <input type="text" name="uniqueid" id="uniqueid" value="" class="input" size="35" tabindex="10"/></label>
                                </p>
                       
                                <p>
                               <label class="login" style = "font-size:20px">Password<br/>
                               <input type="password" name="password" id="password" value="" class="input" size="35" tabindex="20"/></label>
                                </p>
                                <p class="loginsubmit">
                               <button type="submit" name="login" id="terms" class="button" tabindex="100" value="submit"> Log In </button>
                               <input type="hidden" name="resume" value=""/>
                                </p>
                            </div>
                                <div style="clear:both;"> </div>
                            </form>
                          </div>

	               </td>
                      
                    <td width="17" rowspan="2"><img src="spacer_trans.gif" width="17" height="5"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td align="left" valign="top">&nbsp;</td>
              </tr>
              <tr> 
                <td align="left" valign="top" bgcolor="#666666"><img src="spacer_white.gif" width="17" height="1"></td>
              </tr>
              <tr> 
                <td align="left" valign="top">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr align="left" valign="top"> 
       <td width="145" bgcolor="#666666" background="i_internal_leftnav_bg.gif">&nbsp; </td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#666666" align="left" valign="top"> 
    <td colspan="2"> 
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr align="left" valign="top" bgcolor="#FFFFFF"> 
          <td colspan="3"><img src="spacer_white.gif" width="5" height="1"></td>
        </tr>
        <tr align="left" valign="middle"> 
          <td bgcolor="#990000" width="22" valign="top"><img src="spacer_red.gif" width="22" height="28"></td>
          <td bgcolor="#ffffff" width="1" valign="top"><img src="spacer_white.gif" width="1" height="5"></td>
          <td bgcolor="#666666" valign="middle"> 
            <table border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><img src="spacer_darkgrey.gif" width="10" height="5"></td>
                <td nowrap align="center" valign="middle" class="footer">&copy;2017 
                  | Miami University | Oxford, Ohio 45056 | <a class="footer" href="http://www.muohio.edu">www.miamioh.edu</a> 
                  | (513) 529.1809</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
