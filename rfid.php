<?php

/*
Written by Blake Wasung
ISA 406
03/14/2017
*/

// Load the settings from the central config file
require_once 'config.php';
// Load the CAS lib
require_once 'CAS.php';

// Enable debugging
phpCAS::setDebug();

// Initialize phpCAS
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);

// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();

// force CAS authentication
phpCAS::forceAuthentication();
// at this step, the user has been authenticated by the CAS server
// and the user's login name can be read with phpCAS::getUser().

// logout if desired
if (isset($_REQUEST['logout'])) {
        phpCAS::logout();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>RFID System</title>
<link href="favicon.ico" rel="Shortcut Icon"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="rfid.js"></script>
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
        margin-bottom: 0;
        border-radius: 0;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {height: 450px}

/* Set gray background color and 100% height */
.sidenav {
        padding-top: 20px;
        background-color: #f1f1f1;
height: 100%;
}

/* Set black background color, white text and some padding */
footer {
        background-color: #555;
color: white;
padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
        .sidenav {
height: auto;
padding: 15px;
        }
        .row.content {height:auto;}
}
body {
    height:100%;
}
</style>
</head>
<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-left">
                    <li class="active"><a href="#">Home</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="?logout=">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-2 sidenav">
                <a href="">Recently Checked Out</a><br>
                <a href="">Recently Checked In</a><br>
                <a href="">Add New Computer</a>
            </div>
            <div class="col-sm-8 text-left">
                <div class="container">
                    <div class="jumbotron"><h1>Laptop Checkout System</h1>
                        <div id="results">
                            <div class="row">
                                <div class="col-md-6">
                                        <label for="user" class="col-xs-3 col-form-label">Student Id:</label><div class"col-xs-3"><input type="text" id="user"></div>
                                </div>
                                <div class="col-md-6" id="userStatus">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                        <label for="computer" class="col-xs-3 col-form-label">Computer Id:</label><div class"col-xs-3"><input type="text" id="computer"></div>
                                </div>
                                <div class="col-md-6" id="computerStatus">
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn btn-defualt">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="container-fluid text-center">
    </footer>

</body>
</html>

