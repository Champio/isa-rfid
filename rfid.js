var uid = "";
var url = "http://fsb.miamioh.edu/rfid1/rest.php";
function userError(){
    $("#userStatus").removeClass();
    $("#userStatus").html("User not found");
    $("#userStatus").addClass("alert alert-danger col-md-6"); 
}

function userSuccess(data){
    $("#userStatus").removeClass();
    if(data.msg.includes("not found")){
        $("#userStatus").addClass("alert alert-warning col-md-6");
        $("#userStatus").html(data.msg);
        return;
    }
    uid = data.info.UNIQUE_ID;
    $("#userStatus").html(" ");
    for(item in data.info){
        $("#userStatus").append(item+": "+data.info[item]+"<br>");
    }
    if(data.info.USERSTATUS=="Good"){
        $("#userStatus").addClass("alert alert-success col-md-6");
    }else{
        $("#userStatus").addClass("alert alert-warning col-md-6");
    }
    $("#computer").focus().select();
}

function computerError(){
    $("#computerStatus").removeClass();
    $("#computerStatus").html("User not found");
    $("#computerStatus").addClass("alert alert-danger col-md-6"); 
}

function computerSuccess(data){

    $("#computerStatus").removeClass();
    if(data.msg.includes("not found")){
        $("#computerStatus").addClass("alert alert-warning col-md-6");
        $("#computerStatus").html(data.msg);
        return;
    }
    $("#computerStatus").html(" ");
    for(item in data.info){
        $("#computerStatus").append(item+": "+data.info[item]+"<br>");
    }
    console.log(data);
    if(data.info.STATUS){
        if(data.info.STATUS=="AC"){
            $("#computerStatus").addClass("alert alert-success col-md-6");
        }else{
            $("#computerStatus").addClass("alert alert-warning col-md-6");
        }
    }
}

function studentCheck(){
    var userId = $.trim($("#user").val().replace(/[\t\n]+/g, ' '));
        if(!userId){
            $("#userStatus").removeClass();
            $("#userStatus").html("User field must be filled in.");
            $("#userStatus").addClass("alert alert-danger col-md-6");
        }else{
            var settings = {
                "url" : url+"/user?rfid="+userId,
                "method": "GET",
                "success": userSuccess,
                "error": userError
            }
            $.ajax(settings);
        }
}

function computerCheck(){
    var computerId = $.trim($("#computer").val().replace(/[\t\n]+/g, ' '));
    //var computerId = $("#computer").val();
        if(!computerId){
            $("#computerStatus").removeClass();
            $("#computerStatus").html("Computer field must be filled in.");
            $("#computerStatus").addClass("alert alert-danger col-md-6");
        }else{
            var settings = {
                "url" : url+"/computer?rfid="+computerId,
                "method": "GET",
                "success": computerSuccess,
                "error": computerError
            }
            $.ajax(settings);
        }
}
function submitSuccess(data){
    $("#results").html(data.msg);
    if(data.msg=="Something went wrong, please try again"){
        $("#results").addClass("alert alert-danger");
    }else{
        $("#results").addClass("alert alert-success");
    }
}
function submitReq(){
    var dataraw= {"user":uid, "computer":$("#computer").val()};
    var data = JSON.stringify(dataraw);
    var settings = {
        "url": url+"/log",
        "method": "POST",
        "headers":{
            "content-type":"application/json"
        },
        "data": data,
        "success":submitSuccess,
        "fail":function(data){$("#results").html("something went wrong").addClass("alert alert-danger");}
    }
    $.ajax(settings);
}

$(document).ready(function(){
    $("#computer").bind('blur keyup', function(e){
        if(e.type=='blur' || e.keyCode == '13'){
            computerCheck();
        }
    });
    $("#user").bind('blur keyup', function(e){
        if(e.type=='blur' || e.keyCode == '13'){
            studentCheck();
        }
    });
    
    $("#submit").click(submitReq);
    $("#user").focus().select;
});
