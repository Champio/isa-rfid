var url = "http://fsb.miamioh.edu/www/rest.php";
function setData(){
    var data = {"rfid":$("#rfid").val(),
               "serviceTag":$("#serviceTag").val(),
               "type":$("#type").val(),
               "model":$("#model").val(),
               "location":$("#location").val(),
               "ghz":$("#ghz").val(),
               "ram":$("#ram").val(),
               "monitor":$("#monitor").val(),
               "productNumber":$("#productNumber").val(),
               "warranty":$("#warranty").val(),
               "purchaseDate":$("#purchaseDate").val(),
               "cost":$("#cost").val(),
               "description":$("#description").val(),
               "status":$("#status").val()};
    return data;

}

function sendPut(){
    var settings = {
        "url": url+"/computer",
        "method": "PUT",
        "headers":{
            "content-type":"application/json"
        },
        "data":setData(),
        "success":null,
        "fail":null
    };

    $.ajax(settings);

}




$(document).ready(function(){
    sendPut();


});
